package Tree_Traversal;

import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		List<Node> B_tree = new ArrayList<Node>();
		if(node==null){
			return new ArrayList<Node>();
		}
		else {
			B_tree.add(node);
			B_tree.addAll(traverse(node.getLeft()));
			B_tree.addAll(traverse(node.getRigth()));			
			return B_tree;
		}
	}
}
