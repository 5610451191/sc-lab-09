package Tree_Traversal;

public class Node {
	private String node;
	private Node left_node;
	private Node rigth_node;
	
	public Node(String node){
		this(node,null,null);
	}
	public Node(String node,Node left_node,Node rigth_node){
		this.node = node;
		this.left_node = left_node;
		this.rigth_node = rigth_node;
	}
	public String getNode(){
		return node;
	}
	public Node getLeft(){
		return left_node;
	}
	public Node getRigth(){
		return rigth_node;
	}
}
