package Tree_Traversal;

import java.util.List;

public class ReportConsole {
	public void display(Node r,Traversal t){
		List<Node> node = t.traverse(r);
		System.out.println("Traversal with " + t.getClass().getSimpleName());
		for(Node nde : node) {
			System.out.print(nde.getNode()+" ");
		}
		System.out.println();
	}
}
