package Tree_Traversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		List<Node> B_tree;
		if(node==null){
			return new ArrayList<Node>();
		}
		else {
			B_tree = traverse(node.getLeft());
			B_tree.addAll(traverse(node.getRigth()));
			B_tree.add(node);			
			return B_tree;
		}
	}
}
