package Class_Product;

import java.util.ArrayList;
import java.util.Collections;

public class Test_Case {
	public static void main(String[] args){
		
		ArrayList<Product> product = new ArrayList<Product>();
		product.add(new Product("Axe",500));
		product.add(new Product("Tros",350));
		product.add(new Product("Nivea",300));
		product.add(new Product("Vaseline",200));
		for(Product p : product) {
			System.out.println(p.getName() + " " + p.getPrice());
		}
		System.out.println("-------------AFTER SORT---------------");
		Collections.sort(product);
		for(Product p : product) {
			System.out.println(p.getName() + " " + p.getPrice());
		}	
	}
}
