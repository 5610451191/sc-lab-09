package Class_Product;

import Class_Taxable.Taxable;

public class Product implements Comparable<Product>,Taxable {
	private String name;
	private double price;
	
	public Product(String name_cargo,double price_cargo){
		name = name_cargo;
		price = price_cargo;
	}
	
	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}

	@Override
	public int compareTo(Product arg) {
		// TODO Auto-generated method stub
		if (this.price<arg.getPrice()){
			return -1;
		}
		if (this.price>arg.getPrice()) {
			return 1;
		}
		return 0;
	}

	@Override
	public double getTaxt() {
		// TODO Auto-generated method stub
		return (price*7)/100;
	}
	public String toString(){
		return name + "\tprice " + price + "\ttax " +getTaxt();
	}
}
