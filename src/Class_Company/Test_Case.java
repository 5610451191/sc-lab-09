package Class_Company;

import java.util.ArrayList;
import java.util.Collections;

public class Test_Case {
public static void main(String[] args){
		
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("Wut",4200,2000));
		company.add(new Company("Mark",6000,3000));
		company.add(new Company("Pond",5000,2500));
		for(Company c : company) {
			System.out.println(c.getName() + " : " + "\t" + "Earning " + c.getEarning() + "\t" + "Expense " + c.getExpense() + "\t" + "Profit " + c.getProfit());
		}
		Collections.sort(company,new EarningComparator());
		System.out.println("----------AFTER SORT_EarningComparator-----------");
		for(Company c : company) {
			System.out.println(c.getName() + " : " + c.getEarning());
		}
		
		System.out.println("----------AFTER SORT_ExpenseComparator-----------");
		Collections.sort(company,new ExpenseComparator());
		for(Company c : company) {
			System.out.println(c.getName() + " : " + c.getExpense());
		}
		
		System.out.println("----------AFTER SORT_ProfitComparator------------");
		Collections.sort(company,new ProfitComparator());
		for(Company c : company) {
			System.out.println(c.getName() + " : " + c.getProfit());
		}
	}
}

