package Class_Company;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company arg1, Company arg2) {
		// TODO Auto-generated method stub
		if(arg1.getExpense()<arg2.getExpense()) {
			return -1;
		}
		if(arg1.getExpense()>arg2.getExpense()) {
			return 1;
		}
		return 0;
	}
}
