package Class_Company;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company> {

	@Override
	public int compare(Company arg1, Company arg2) {
		// TODO Auto-generated method stub
		if(arg1.getProfit()<arg2.getProfit()) {
			return -1;
		}
		if(arg1.getProfit()>arg2.getProfit()) {
			return 1;
		}
		return 0;
	}

}
