package Class_Company;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company arg1, Company arg2) {
		// TODO Auto-generated method stub
		if(arg1.getEarning()<arg2.getEarning()) {
			return -1;
		}
		if(arg1.getEarning()>arg2.getEarning()) {
			return 1;
		}
		return 0;
	}	
}
