package Class_Company;

import Class_Taxable.Taxable;

public class Company implements Taxable{
	private String name;
	private double earning;
	private double expense;
	private double profit;
	
	public Company(String name,double earning,double expense) {
		this.name = name;
		this.earning = earning;
		this.expense = expense;
		
		profit = earning - expense;
	}
	
	public String getName(){
		return name;
	}
	public double getEarning(){
		return earning;
	}
	public double getExpense(){
		return expense;
	}
	public double getProfit(){
		return profit;
	}

	@Override
	public double getTaxt() {
		// TODO Auto-generated method stub
		if (getExpense()>getEarning()) {
			return 0;
		}
		else {
			return ((getEarning()-getExpense())*0.3);
		}
	}
	public String toString(){
		return name + "\tearning " + earning + "\texpense " + expense + "\tprofit " + profit + "\ttax " +getTaxt();
	}
}
