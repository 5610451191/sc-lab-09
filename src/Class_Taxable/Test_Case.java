package Class_Taxable;

import java.util.ArrayList;
import java.util.Collections;

import Class_Company.Company;
import Class_Company.EarningComparator;
import Class_Company.ExpenseComparator;
import Class_Company.ProfitComparator;
import Class_Person.Person;
import Class_Product.Product;

public class Test_Case {
	public static void main(String[] args){
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new Product("Axe",500));
		tax.add(new Company("Mark",6000,3000));
		tax.add(new Product("Nivea",300));
		tax.add(new Company("Pond",5000,2500));
		tax.add(new Person("Wut",4200));
		tax.add(new Product("Care",200));
		tax.add(new Person("Pond",5000));
		tax.add(new Company("Wut",4200,2000));
		tax.add(new Person("Black",4500));
		tax.add(new Product("Tros",350));
		for(Taxable taxa : tax){
			System.out.println(taxa);
		}
		System.out.println("-------------AFTER SORT---------------");
		Collections.sort(tax,new Conparator());
		for(Taxable taxa : tax){
			System.out.println(taxa);
		}
	}
}
