package Class_Person;

import Class_Taxable.Taxable;

public class Person implements Comparable<Person>,Taxable  {
	private String name;
	private Double money;

	public Person(String aname,double amoney){
		name = aname;
		money = amoney;
	}
	
	public String getName(){
		return name;
	}
	public double getMoney(){
		return money;
	}

	@Override
	public int compareTo(Person arg) {
		// TODO Auto-generated method stub
		if (this.money<arg.getMoney()){
			return -1;
		}
		if (this.money>arg.getMoney()) {
			return 1;
		}
		return 0;
	}

	@Override
	public double getTaxt() {
		// TODO Auto-generated method stub
		return (money*7)/100;
	}
	
	public String toString(){
		return name+"\tsalary " + money + "\ttax " +getTaxt();
	}
}
