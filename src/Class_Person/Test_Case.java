package Class_Person;

import java.util.ArrayList;
import java.util.Collections;

public class Test_Case {
	public static void main(String[] args){
		
		ArrayList<Person> person = new ArrayList<Person>();
		person.add(new Person("Wut",4200));
		person.add(new Person("Black",4000));
		person.add(new Person("Pond",5000));
		for(Person p : person) {
			System.out.println(p.getName() + " " + p.getMoney());
		}
		System.out.println("-------------AFTER SORT---------------");
		Collections.sort(person);
		for(Person p : person) {
			System.out.println(p.getName() + " " + p.getMoney());
		}
	}
}
